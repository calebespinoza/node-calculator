class CalculatorLibrary{
    Add(num1, num2){
        return num1 + num2;
    }
    
    Subtract(num1, num2){
        return num1 - num2;
    }
    
    Multiply(num1, num2){
        return num1 * num2;
    }
    
    Divide(num1, num2){
        return num1 / num2;
    }
}

module.exports = CalculatorLibrary;

/*module.exports = { 
    Add, Subtract, Multiply, Divide
}*/


/*
 Create a nodejs project you can use this(https://github.com/contentful/the-example-app.nodejs) or your custom project
- Apply a mimification of your project using a gulp plugin for example: (https://www.npmjs.com/package/gulp-minify, https://www.npmjs.com/package/gulp-uglify)
- Create gulp task for create a zip of you distribution/minified folder (web project)
- Upload npm package to nexus (https://blog.sonatype.com/using-nexus-3-as-your-repository-part-2-npm-packages)
*/